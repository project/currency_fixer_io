<?php

namespace Drupal\currency_fixer_io\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\currency_fixer_io\Service\FixerIo;

/**
 * Provides configuration form for fixer.io currency exchange rates provider.
 */
class CurrencyFixerIoSettingsForm extends ConfigFormBase {

  /**
   * Fixer.io service.
   *
   * @var \Drupal\currency_fixer_io\Service\FixerIo
   */
  protected $fixerIo;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [FixerIo::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('currency_fixer_io.fixer_io')
    );
  }

  /**
   * Constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, FixerIo $fixerIo) {
    parent::__construct($config_factory);

    $this->fixerIo = $fixerIo;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'currency_fixer_io_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config(FixerIo::CONFIG_NAME)->get();

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fixer.io API key'),
      '#default_value' => $settings['api_key'],
    ];

    if (!empty($settings['api_key'])) {
      $form['frequency'] = [
        '#type' => 'select',
        '#title' => $this->t('Update rates'),
        '#options' => [
          0 => $this->t('live'),
          60 => $this->t('60 seconds'),
          600 => $this->t('10 minutes'),
          3600 => $this->t('hourly'),
          84600 => $this->t('daily'),
          604800 => $this->t('weekly'),
          2538000 => $this->t('monthly'),
        ],
        '#default_value' => $settings['frequency'],
      ];

      $form['use_ssl'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Use SSL'),
        '#description' => $this->t('Available on any of the paid plans.'),
        '#default_value' => $settings['use_ssl'],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(FixerIo::CONFIG_NAME);
    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
