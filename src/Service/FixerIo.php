<?php

namespace Drupal\currency_fixer_io\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\Exception\ClientException as GuzzleException;

/**
 * Provides methods to call Fixer.io webapi.
 */
class FixerIo {

  const CONFIG_NAME = 'currency_fixer_io.settings';
  const ENDPOINT_URL = 'data.fixer.io/api/';

  /**
   * The fixer.io API base URL.
   *
   * @var string
   */
  protected $apiUrl;

  /**
   * The fixer.io service API key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Logger for this channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Drupal state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Currency refresh time condition.
   *
   * @var int
   */
  protected $refreshCondition;

  /**
   * Currency exchange rates.
   *
   * @var array
   */
  protected $rates = [];

  /**
   * Object constructor.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Client $httpClient, LoggerChannelFactoryInterface $loggerFactory, StateInterface $state) {
    $config = $configFactory->get(self::CONFIG_NAME);

    $this->apiUrl = $config->get('use_ssl') ? 'https://' . self::ENDPOINT_URL : 'http://' . self::ENDPOINT_URL;
    $this->apiKey = $config->get('api_key');
    $this->httpClient = $httpClient;
    $this->logger = $loggerFactory->get('currency_fixer_io');
    $this->state = $state;

    $frequency = $config->get('frequency');
    $this->refreshCondition = $frequency ? \Drupal::time()->getRequestTime() - $frequency : 0;
  }

  /**
   * Get currency rates data.
   *
   * @return array
   *   Exchange rates relative to base currency.
   */
  protected function getRatesData() {
    if (empty($this->rates)) {
      $rates_data = $this->state->get('currency_fixer_io_rates_data');

      // Update rates data if required.
      if (empty($rates_data['last_update']) || empty($this->refreshCondition) || $rates_data['last_update'] < $this->refreshCondition) {
        $rates_data = $this->latest();
        if (!empty($rates_data)) {
          $rates_data['last_update'] = \Drupal::time()->getRequestTime();
          $this->state->set('currency_fixer_io_rates_data', $rates_data);
        }
      }
      if (isset($rates_data['rates']) && isset($rates_data['base'])) {
        $rates_data['rates'][$rates_data['base']] = 1;
        $this->rates = $rates_data['rates'];
      }
    }

    return $this->rates;
  }

  /**
   * Log error.
   *
   * @param string $method
   *   The called method.
   * @param string $error
   *   The error message to log.
   */
  protected function logError($method, $error) {
    $this->logger->error('Exception occurred during fixer.io request. Operation: @operation, Error: @error',
      [
        '@operation' => $method,
        '@error' => $error,
      ]
    );
  }

  /**
   * Call the fixer.io API.
   *
   * @param string $method
   *   The method to call.
   * @param array $variables
   *   The variables passed to the method.
   *
   * @return array
   *   Response from the API.
   */
  public function __call($method, array $variables) {
    $output = [];

    if (empty($this->apiKey)) {
      $this->logError($method, 'Please set your API key on the module settings page.');
      return $output;
    }

    $parameters = [
      'access_key' => $this->apiKey,
    ];
    if (!empty($variables[0]) && is_array($variables[0])) {
      foreach ($variables[0] as $name => $value) {
        $parameters[$name] = $value;
      }
    }

    // Setup HTTP client options.
    $options = [
      'query' => $parameters,
    ];

    try {
      $response = $this->httpClient->request('GET', $this->apiUrl . $method, $options);
    }
    catch (GuzzleException $e) {
      $this->logError($method, (string) $e->getMessage());
    }

    if (isset($response)) {
      $response_arr = json_decode((string) $response->getBody(), TRUE);
      if ($response_arr['success']) {
        $output = $response_arr;
      }
      else {
        $error_message = $response_arr['error']['code'] . '(' . $response_arr['error']['type'] . ')';
        if (isset($response_arr['error']['info'])) {
          $error_message .= ': ' . $response_arr['error']['info'];
        }
        $this->logError($method, $error_message);
      }
    }
    return $output;
  }

  /**
   * Get available currency codes.
   *
   * @return array
   *   Array of currency names keyed by currency codes.
   */
  public function getCurrencies() {
    $output = [];
    $response = $this->symbols();
    if (!empty($response['symbols'])) {
      $output = $response['symbols'];
    }
    return $output;
  }

  /**
   * Get exchange rates for the specified currencies.
   *
   * @param string $from
   *   The source currency code.
   * @param string[] $to
   *   Array of destination currency codes.
   *
   * @return array
   *   Array of rates keyed by destination currency codes.
   */
  public function getRates($from, array $to) {
    $output = [];
    $rates = $this->getRatesData();
    if (!empty($rates[$from])) {
      foreach ($to as $to_code) {
        if (isset($rates[$to_code])) {
          $output[$to_code] = $rates[$to_code] / $rates[$from];
        }
      }
    }

    return $output;
  }

}
