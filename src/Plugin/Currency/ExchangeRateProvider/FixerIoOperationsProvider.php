<?php

namespace Drupal\currency_fixer_io\Plugin\Currency\ExchangeRateProvider;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\plugin\PluginOperationsProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides operations for the fixed rates exchange rate provider.
 */
class FixerIoOperationsProvider implements PluginOperationsProviderInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The redirect destination.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Constructs a new instance.
   *
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination.
   */
  public function __construct(RedirectDestinationInterface $redirect_destination) {
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('redirect.destination'));
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations($plugin_id) {
    $operations['configure'] = [
      'title' => $this->t('Configure'),
      'query' => $this->redirectDestination->getAsArray(),
      'url' => new Url('currency.exchange_rate_provider.fixer_io.configure'),
    ];

    return $operations;
  }

}
