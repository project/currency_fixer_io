<?php

namespace Drupal\currency_fixer_io\Plugin\Currency\ExchangeRateProvider;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides exchange rates from fixer.io.
 *
 * @CurrencyExchangeRateProvider(
 *   id = "currency_fixer_io",
 *   label = @Translation("Fixer.io"),
 *   operations_provider = "\Drupal\currency_fixer_io\Plugin\Currency\ExchangeRateProvider\FixerIoOperationsProvider"
 * )
 */
class FixerIoExchangeRateProvider extends PluginBase implements ExchangeRateProviderInterface, ContainerFactoryPluginInterface {

  const RATES_CONFIG_NAME = 'currency.exchanger.fixer_io';
  const TABLE_NAME = 'currency_fixer_io';

  /**
   * Fixer.io service.
   *
   * @var \Drupal\currency_fixer_io\Service\FixerIo
   */
  protected $fixerIo;

  /**
   * Constructs a new class instance.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ConfigFactoryInterface $config_factory, FixerIo $fixerIo) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fixerIo = $fixerIo;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('currency_fixer_io.fixer_io')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function load($sourceCurrencyCode, $destinationCurrencyCode) {
    $rate_data = $this->fixerIo->getRates($sourceCurrencyCode, [$destinationCurrencyCode]);

    if (isset($rate_data[$destinationCurrencyCode])) {
      return new ExchangeRate($sourceCurrencyCode, $destinationCurrencyCode, $rate_data[$destinationCurrencyCode]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $currencyCodes) {
    foreach ($currencyCodes as $from => $to_arr) {
      $rate_data = $this->fixerIo->getRates($from, $to_arr);
      foreach ($to_arr as $to) {
        if (isset($rate_data[$to])) {
          $output[$from][$to] = new ExchangeRate($from, $to, $rate_data[$to]);
        }
      }
    }

    return $output;
  }

}
